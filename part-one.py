#!/usr/bin/env python3

import os

class Cube:
    cubes_xy = {}
    cubes_xz = {}
    cubes_yz = {}

    def map(function):
        return [function(cube) for cube_z in Cube.cubes_xy.values() for cube in cube_z.values()]

    def for_all(function):
        [function(cube) for cube_z in Cube.cubes_xy.values() for cube in cube_z.values()]

    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        self.neighbours = []
        if (x,y) not in Cube.cubes_xy:
            Cube.cubes_xy[(x,y)] = {z: self}
        else:
            Cube.cubes_xy[(x,y)][z] = self
        if (x,z) not in Cube.cubes_xz:
            Cube.cubes_xz[(x,z)] = {y: self}
        else:
            Cube.cubes_xz[(x,z)][y] = self
        if (y,z) not in Cube.cubes_yz:
            Cube.cubes_yz[(y,z)] = {x: self}
        else:
            Cube.cubes_yz[(y,z)][x] = self

    def hook_up_neighbours(self):
        xy = Cube.cubes_xy[(self.x, self.y)]
        xz = Cube.cubes_xz[(self.x, self.z)]
        yz = Cube.cubes_yz[(self.y, self.z)]
        if (self.z - 1) in xy:
            self.neighbours.append(xy[self.z-1])
        if (self.z + 1) in xy:
            self.neighbours.append(xy[self.z+1])
        if (self.y - 1) in xz:
            self.neighbours.append(xz[self.y-1])
        if (self.y + 1) in xz:
            self.neighbours.append(xz[self.y+1])
        if (self.x - 1) in yz:
            self.neighbours.append(yz[self.x-1])
        if (self.x + 1) in yz:
            self.neighbours.append(yz[self.x+1])

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n")

    for line in data:
        Cube(*[int(v) for v in line.split(",")])

    Cube.for_all(lambda cube: cube.hook_up_neighbours())

    answer = sum(Cube.map(lambda cube: 6 - len(cube.neighbours)))

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
